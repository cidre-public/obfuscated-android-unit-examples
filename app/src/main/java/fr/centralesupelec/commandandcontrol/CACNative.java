package fr.centralesupelec.commandandcontrol;

public class CACNative {
        public String url = "cc.url";
        public int port = 4242;
        native public void sendCommand(byte[] buf, int length);
    }

   /* JNICALL Java_CommandAndControl_sendCommand(JNIEnv*env,
                                               jobject thisPtr, jbyteArray buf, jint length) {

        byte * bufData = env->GetByteArrayElements(buf, NULL);
        jint port = env->GetIntField(thisPtr,
                env->GetFieldId(env->GetObjectClass(thisPtr),
                        "port", "I"));

        // Use libc functions to send the packet
   */
