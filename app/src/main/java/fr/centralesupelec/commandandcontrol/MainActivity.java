package fr.centralesupelec.commandandcontrol;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        try {

        CommandAndControl cc = new CommandAndControl();

        cc.sendCommand("Coucou\n".getBytes(), 7);



        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
