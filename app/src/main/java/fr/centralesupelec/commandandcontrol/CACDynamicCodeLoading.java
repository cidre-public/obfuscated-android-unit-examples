package fr.centralesupelec.commandandcontrol;

import android.util.Log;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import dalvik.system.PathClassLoader;

public class CACDynamicCodeLoading {

    private File file = new File("TODO.txt");

    void invokeDynamically() {
        try {
            /* Loads the implementation of C&C */
            PathClassLoader pcl = new PathClassLoader(file.getPath(), null);
            Class<?> clazz = pcl.loadClass("CommandAndControl", this);
            Log.i("CL", "Loaded class from dex: " + clazz);
            Constructor<?> c = clazz.getConstructor();
            Object o = null;

            o = c.newInstance();

            Method m = clazz.getMethod("send",
                    byte[].class, int.class);
            m.invoke(o, "TODO"); // CommandAndControl.send(....)

        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

}