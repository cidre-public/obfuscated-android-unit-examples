package fr.centralesupelec.commandandcontrol;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;


class CommandAndControl {
    public String url = "127.0.0.1:5050";
    public int port = 4242;
    public void sendCommand(byte[] buf, int length) throws IOException {
        DatagramPacket dp = new DatagramPacket(buf, length,
                InetAddress.getByName(this.url),
                this.port);
        new DatagramSocket().send(dp);
    }
}
